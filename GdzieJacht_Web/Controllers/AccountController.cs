﻿using GdzieJacht_Web.GdzieJachtService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GdzieJacht_Web.Models;
using GdzieJacht_Web.Infrastructure.Contract;

namespace GdzieJacht_Web.Controllers
{
    public class AccountController : Controller
    {
        private IAuthenticationProvider authProvider;

        public AccountController(IAuthenticationProvider authProvider)
        {
            this.authProvider = authProvider;
        }

        public ActionResult Account(string id = null)
        {
            if (id != null && id == "logout")
                ViewBag.Logout = "logout";
            ViewBag.LoginModel = new LoginViewModel();
            ViewBag.RegisterModel = new RegisterViewModel();
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            ViewBag.Action = "login";
            if (ModelState.IsValid)
            {
                if (authProvider.Authenticate(model.Login, model.Password))
                {
                    return Redirect(returnUrl ?? Url.Action("MoveToTrip", "Trip"));
                }
                else
                {
                    ModelState.AddModelError("", "Nieprawidłowa nazwa użytkownika lub hasło");
                    return View("Account");
                }
            }
            ViewBag.LoginModel = model;
            ViewBag.RegisterModel = new RegisterViewModel();
            return View("Account");
        }

        public ActionResult Logout(string id = null)
        {
            if (id != null)
                ViewBag.Logout = "logout";
            authProvider.Logout();
            HttpCookie cookie1 = new HttpCookie(System.Web.Security.FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);
            return View("Account");
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel model, string returnUrl)
        {
            ViewBag.Action = "register";
            if (ModelState.IsValid)
            {
                if (authProvider.Register(model.RegisterLogin, model.RegisterPassword, model.Email, model.PhoneNumber))
                    return Redirect(returnUrl ?? Url.Action("MoveToTrip", "Trip"));
                else
                {
                    ModelState.AddModelError("", "Użytkownik o podanym loginie już istnieje");
                    return View("Account");
                }
            }
            ViewBag.LoginModel = new LoginViewModel();
            ViewBag.RegisterModel = model;
            return View("Account");
        }
    }
}
