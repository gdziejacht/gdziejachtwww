﻿using GdzieJacht_Web.GdzieJachtService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GdzieJacht_Web.Controllers
{
    [Authorize]
    public class WebMailController : Controller
    {
        //nie wiem z czym będzie powiązany widok poczty, więc na razie daję usera
        private User user;

        public WebMailController(User user)
        {
            this.user = user;
        }

        public ViewResult WebMail()
        {
            return View(user);
        }

        //testowa metoda przejścia do widoku WebMail
        //wykorzystywana w widoku, z ktorego chcemy przejść do WebMail, np w Friends
        public ViewResult MoveToWebMail()
        {
            return View("~/Views/WebMail/WebMail.cshtml");
        }



    }
}
