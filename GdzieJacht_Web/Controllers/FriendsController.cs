﻿using GdzieJacht_Web.GdzieJachtService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GdzieJacht_Web.Models;

namespace GdzieJacht_Web.Controllers
{
    [Authorize]
    public class FriendsController : Controller
    {
        private User loggedUser;
        private IGdzieJachtService gdzieJachServiceClient;
        private AuthenticationInfo ai = new AuthenticationInfo { Username = "test", Password = "test" };

        public FriendsController(IGdzieJachtService gdzieJachServiceClient)
        {
            this.gdzieJachServiceClient = gdzieJachServiceClient;
            loggedUser = (User)System.Web.HttpContext.Current.Session["User"];
        }

        public ActionResult Friends()
        {
            if (loggedUser == null)
                return Redirect(Url.Action("Logout", "Account", new { id = "SessionExpired" }));
            return View();
        }

        public ActionResult AddFriend(int friendID)
        {
            if (loggedUser == null)
                return JavaScript("onFailure();");
            gdzieJachServiceClient.AddFriend(loggedUser.user_id, friendID, ai);
            return View("GetFriends", gdzieJachServiceClient.GetFriends(loggedUser.user_id, ai));
        }

        public ActionResult RemoveFriend(int friendID)
        {
            if (loggedUser == null)
                return JavaScript("onFailure();");
            gdzieJachServiceClient.RemoveFriend(loggedUser.user_id, friendID, ai);
            return View("GetFriends", gdzieJachServiceClient.GetFriends(loggedUser.user_id, ai));
        }

        public ActionResult SearchFriends(string userName)
        {
            if (loggedUser == null)
                return JavaScript("onFailure();");
            IEnumerable<User> result = null;
            if (!string.IsNullOrEmpty(userName))
            {
                IEnumerable<int> friendsIDs = from user in gdzieJachServiceClient.GetFriends(loggedUser.user_id, ai)
                                              select user.user_id;
                result = from user in gdzieJachServiceClient.GetUsersList(ai).Where(u => !friendsIDs.Contains(u.user_id) && u.user_id != loggedUser.user_id)
                         orderby user.user_name
                         where user.user_name.ToLower().Contains(userName.ToLower())
                         select user;
                ViewBag.UserName = userName;
            }
            return PartialView(result);
        }

        public ActionResult GetFriends()
        {
            if (loggedUser == null)
                return JavaScript("onFailure();");
            return PartialView(gdzieJachServiceClient.GetFriends(loggedUser.user_id, ai));
        }
    }
}
