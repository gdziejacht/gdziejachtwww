﻿using GdzieJacht_Web.GdzieJachtService;
using GdzieJacht_Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GdzieJacht_Web.Controllers
{
    [Authorize]
    public class UserProfileController : Controller
    {

        private IGdzieJachtService gdzieJachServiceClient;

        private AuthenticationInfo ai = new AuthenticationInfo { Username = "test", Password = "test" };

        private User loggedUser;
        
        public UserProfileController(IGdzieJachtService gdzieJachServiceClient)
        {
            this.gdzieJachServiceClient = gdzieJachServiceClient;
            this.ai = new AuthenticationInfo { Username = "test", Password = "test" };
            this.loggedUser = (User)System.Web.HttpContext.Current.Session["User"];
        }

        public ActionResult UserProfile()
        {
            if (loggedUser != null)
            {
                UserAndPhotoViewModel userAndPhotoViewModel = new UserAndPhotoViewModel();
                userAndPhotoViewModel.user = loggedUser;
                userAndPhotoViewModel.userPhoto = retrieveRandomUserPhoto();
                return View(userAndPhotoViewModel);
            }
            else
            {
                return Redirect(Url.Action("Logout", "Account", new { id = "SessionExpired" }));
            }
        }

        public ActionResult MoveToWebUserProfile()
        {
            if (loggedUser != null)
                return View("~/Views/UserProfile/UserProfile.cshtml");
            else
                return Redirect(Url.Action("Logout", "Account", new { id = "SessionExpired" }));
        }

        private Photo retrieveRandomUserPhoto()
        {
            Photo userPhoto = null;
            try
            {
                Photo[] userPhotos = gdzieJachServiceClient.GetUserPhotos(loggedUser.user_id, ai);
                if (userPhotos.Length > 0)
                {
                    Random random = new Random();
                    userPhoto = userPhotos[random.Next(userPhotos.Length)];
                }
            }
            catch (Exception ignored) {}
           
            return userPhoto;
        }
    }
}
