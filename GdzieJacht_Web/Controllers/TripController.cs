﻿using GdzieJacht_Web.GdzieJachtService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;

namespace GdzieJacht_Web.Controllers
{
    public class TripController : Controller
    {
        private GdzieJachtServiceClient client;
        private Trip trip;
        private AuthenticationInfo info;
        private User loggedUser;

        public TripController(GdzieJachtServiceClient client, Trip trip, AuthenticationInfo info)
        {
            this.client = client;
            this.trip = trip;
            this.info = info;
            loggedUser = (User)System.Web.HttpContext.Current.Session["User"];
        }

        public ViewResult Trip()
        {
            return View(new List<Trip> {trip});
        }

        public ActionResult GetTrip(Int32 id)
        {
            //TODO: Dodać weryfikację czy trasa należy do użytkownika
            if (loggedUser != null)
            {
                Trip trip = null;
                try
                {

                    trip = client.GetTrip(id, this.info);
                }
                catch (FaultException ex)
                {
                    return HttpNotFound();
                }

                return View(trip);
            }
            else
            {
                return Redirect(Url.Action("Logout", "Account", new { id = "SessionExpired" }));
            }
        }

        public PartialViewResult GetCheckpoints(Int32 id)
        {
            var auth = new AuthenticationInfo
            {
                Username = "test",
                Password = "test"
            };

            var trip = client.GetTrip(id, auth);

            var checkpoints = client.GetTripCheckpoints(trip, auth);

            return PartialView(checkpoints);
        }

        //testowa metoda przejścia do widoku Trip
        //wykorzystywana w widoku, z ktorego chcemy przejść do Trip, np w Loginie
        public ActionResult MoveToTrip()
        {
            if (loggedUser != null)
            {
                var auth = new AuthenticationInfo
                {
                    Username = "test",
                    Password = "test"
                };
                var trips = client.GetTripListForUser(loggedUser.user_id, auth);
                if (trips == null || !trips.Any())
                    return View("~/Views/Trip/NoTrips.cshtml");

                var userLanguage = System.Globalization.CultureInfo.CurrentUICulture;
                // return View("~/Views/Trip/Trip.cshtml", trips.OrderByDescending(c => c.trip_date.ToString().ToString(userLanguage)).ToList());
                return View("~/Views/Trip/Trip.cshtml", trips.OrderByDescending(c => c.trip_date.ToString().ToString(System.Globalization.CultureInfo.InvariantCulture)).ToList());
            }
            else
            {
                return Redirect(Url.Action("Logout", "Account", new { id = "SessionExpired" }));
            }
        }

        //reszta kodu
        public ActionResult GetPoints(Int32 id)
        {
            var auth = new AuthenticationInfo
            {
                Username = "test",
                Password = "test"
            };

            var trip = id == 0 ? new Trip() : client.GetTrip(id, auth);

            var checkpoints = client.GetTripCheckpoints(trip, auth);

          
            return Json(checkpoints, JsonRequestBehavior.AllowGet);
        }
    }
}
