﻿using GdzieJacht_Web.GdzieJachtService;
using GdzieJacht_Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;

namespace GdzieJacht_Web.Controllers
{
    [Authorize]
    public class PhotoController : Controller
    {
        private User loggedUser;
        private IGdzieJachtService gdzieJachtServiceClient;
        private AuthenticationInfo ai = new AuthenticationInfo { Username = "test", Password = "test" };
        private Photo[] photos;

        public PhotoController(IGdzieJachtService gdzieJachtServiceClient)
        {
            this.gdzieJachtServiceClient = gdzieJachtServiceClient;
            loggedUser = (User)System.Web.HttpContext.Current.Session["User"];
        }

        public ActionResult Friends_To_Choose()
        {
            if (loggedUser != null)
            {
                ViewBag.LoggedUserName = loggedUser.user_name;
                ViewBag.LoggedUserID = loggedUser.user_id;
                ViewBag.Error = "";
                string message1 = "Nie dodałeś jeszcze znajomych";
                string message2 = "Żaden z Twoich znajomych nie ma zdjęć";

                List<User> users_with_photos = new List<User>();
                User[] users;

                try
                {
                    users = gdzieJachtServiceClient.GetFriends(loggedUser.user_id, ai);

                    if (users.Count() != 0 && users != null)
                    {
                        for (int i = 0; i < users.Count(); i++)
                        {   
                            if (gdzieJachtServiceClient.GetUserPhotoCount(users[i].user_id, ai)!=0)
                            {
                                users_with_photos.Add(users[i]);
                            }
                        }

                        if (users_with_photos.Count == 0 || users_with_photos == null)
                        {
                            throw new Exception(message2);
                        }
                    }
                    else
                    {
                        throw new Exception(message1);
                    }
                }
                catch (Exception e)
                {
                    ExceptionDetail exdetail = new ExceptionDetail(e);
                    FaultException<ExceptionDetail> exception = new FaultException<ExceptionDetail>(exdetail);
                    //var except = e as FaultException<ExceptionDetail>;
                    //var exceptiondetail = except.Detail.Type;
                    if (exdetail.Type == "GdzieJachtService.CustomException")
                    {
                        ViewBag.Error = "Nie dodałeś jeszcze znajomych lub Twoi znajomi nie mają zdjęć.";
                    }
                    ViewBag.Error = e.Message;
                }

                return PartialView(users_with_photos);
            }
            else
            {
                //return Redirect("~/Account/Account");
                return Redirect(Url.Action("Logout", "Account", new { id = "SessionExpired" }));
            }
        }

        public ActionResult Friends_Gallery(int? friend_id, int page=1)
        {
            if (loggedUser != null)
            {
                ViewBag.ErrorGallery = "";
                string message = "Brak zdjęć użytkownika";
                int count_total;
                int count;

                List<Photo> photo_list = new List<Photo>();
                try
                {
                    count = gdzieJachtServiceClient.GetUserPhotoCount((int)friend_id, ai);
                    count_total = (int)Math.Ceiling((decimal)count / 10);

                    if (count>=10)
                    {
                        for (int i = 0; i <= count_total+1; i++)
                        {
                            photos = gdzieJachtServiceClient.Get10UserPhotos((int)friend_id, i, ai);

                            for (int j = 0; j < photos.Count(); j++)
                            {
                                if (photos[j] != null)
                                {
                                    photo_list.Add(photos[j]);
                                }
                            }
                        }
                    }

                    else
                    {
                        photo_list = gdzieJachtServiceClient.GetUserPhotos((int)friend_id, ai).ToList();
                    }

                    if (photo_list.Count != 0)
                    {
                        PhotoViewModel viewModel = new PhotoViewModel
                        {
                            Photos = photo_list,
                            //.OrderBy(p => p.photo_id)
                            //.Skip((page - 1) * pageSize)
                            //.Take(pageSize),
                            PagingInfo = new PagingInfo
                            {
                                CurrentPage = page,
                                ItemsPerPage = 10,
                                TotalItems = count_total
                            }
                        };

                        return PartialView(viewModel); 
                       
                    }
                    else
                    {
                        throw new Exception(message);
                    }
                }

                catch (Exception e)
                {
                    ExceptionDetail exdetail = new ExceptionDetail(e);
                    FaultException<ExceptionDetail> exception = new FaultException<ExceptionDetail>(exdetail);
                    //var except = e as FaultException<ExceptionDetail>;
                    //var exceptiondetail = except.Detail.Type;
                    if (exdetail.Type == "GdzieJachtService.CustomException")
                    {
                        ViewBag.ErrorGallery = e.Message;
                    }
                    ViewBag.ErrorGallery = e.Message;
                    return PartialView();
                }
            }
            else
            {
                //return Redirect("~/Account/Account");
                return JavaScript("onFailure();");
            }
        }

        public ActionResult Photo()
        {
            if (loggedUser != null)
            {
                return View();
            }
            else
            {
                //return Redirect("~/Account/Account");
                return Redirect(Url.Action("Logout", "Account", new { id = "SessionExpired" }));
            }

        }

    }
}
