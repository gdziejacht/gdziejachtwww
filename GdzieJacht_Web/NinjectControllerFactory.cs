﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using System.Web.Mvc;
using System.Web.Routing;
using GdzieJacht_Web.Infrastructure.Contract;
using GdzieJacht_Web.Infrastructure.Implementation;
using GdzieJacht_Web.GdzieJachtService;

namespace GdzieJacht_WebApp
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            //powiązania
            ninjectKernel.Bind<IAuthenticationProvider>().To<AuthenticationProvider>();
            ninjectKernel.Bind<IGdzieJachtService>().To<GdzieJachtServiceClient>();
        }
    }
}