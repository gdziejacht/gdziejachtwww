﻿using GdzieJacht_Web.GdzieJachtService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GdzieJacht_Web.Models
{
    public class PhotoViewModel
    {
        public IEnumerable<Photo> Photos { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}