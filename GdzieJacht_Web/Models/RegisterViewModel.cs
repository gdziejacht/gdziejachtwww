﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GdzieJacht_Web.Models
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Proszę podać nazwę użytkownika")]
        [Display(Name = "Login")]
        public string RegisterLogin { get; set; }

        [Required(ErrorMessage = "Proszę podać hasło")]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string RegisterPassword { get; set; }

        [Required(ErrorMessage = "Proszę powtórzyć hasło")]
        [DataType(DataType.Password)]
        [Display(Name = "Powtórz hasło")]
        [Compare("RegisterPassword", ErrorMessage="Zawartość pól \"Hasło\" i \"Powtórz hasło\" jest różna")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Proszę podać adres e-mail")]
        [EmailAddress]
        [Display(Name = "Adres e-mail")]
        public string Email { get; set; }

        [Display(Name = "Nr telefonu")]
        [Phone]
        public string PhoneNumber { get; set; }
    }
}