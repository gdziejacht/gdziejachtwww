﻿using GdzieJacht_Web.GdzieJachtService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GdzieJacht_Web.Models
{
    public class UserAndPhotoViewModel
    {
        public User user { get; set; }

        public Photo userPhoto { get; set; }
    }
}