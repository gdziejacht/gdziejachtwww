﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using GdzieJacht_Web.Infrastructure.Contract;
using GdzieJacht_Web.GdzieJachtService;

namespace GdzieJacht_Web.Infrastructure.Implementation
{
    public class AuthenticationProvider : IAuthenticationProvider
    {
        private IGdzieJachtService gdzieJachServiceClient;

        public AuthenticationProvider(IGdzieJachtService gdzieJachServiceClient)
        {
            this.gdzieJachServiceClient = gdzieJachServiceClient;
        }

        public bool Authenticate(string username, string password)
        {
            AuthenticationInfo ai = new AuthenticationInfo();
            User user;
            ai.Password = "test";
            ai.Username = "test";
            try
            {
                user = gdzieJachServiceClient.GetUser(username, password, ai);
                if (user != null)
                    HttpContext.Current.Session["User"] = user;
            }
            catch (Exception e)
            {
                var except = e as System.ServiceModel.FaultException<System.ServiceModel.ExceptionDetail>;
                var exceptiondetail = except.Detail.Type;
                if (exceptiondetail == "GdzieJachtService.CustomException")
                {
                    // wyświetl komunikat do użytkownika

                }
                return false;
            }
            FormsAuthentication.SetAuthCookie(username, false);
            return true;
        }

        public bool Register(string login, string password, string email, string phoneNumber)
        {
            RegisterContract rc = new RegisterContract
            {
                Login = login,
                Password = password,
                Email = email,
                PhoneNumber = phoneNumber
            };
            try
            {
                User user = gdzieJachServiceClient.Register(rc);
                if (user != null)
                    HttpContext.Current.Session["User"] = user;
            }
            catch (Exception)
            {
                return false;
            }
            FormsAuthentication.SetAuthCookie(login, false);
            return true;
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}