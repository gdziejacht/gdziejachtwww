﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GdzieJacht_Web.Infrastructure.Contract
{
    public interface IAuthenticationProvider
    {
        bool Authenticate(string login, string password);
        bool Register(string login, string password, string email, string phoneNumber);
        void Logout();
    }
}
